# ReactJS quarantine sorting project

Simple project used to discover React Native and review some sorting algorithms

[![screenshot of the app](./assets/screenshot.png "screenshot of the app")](https://etiennepericat.gitlab.io/reactjs-sorting/)

## live demo : [reactjs-sorting](https://etiennepericat.gitlab.io/reactjs-sorting/)

## Implemented algorithms

- Bubble
- Cocktail
- QuickSort (development on-going)

## How does it works 

Basically we run the algorithm and populate an array of "animations" that will be run afterwards. 

The **"real" processing time** displayed is the process time without animations. 

Each iteration of the algorithm is played with the given timeout. 