import { Component } from "react";
import React from 'react';
import { grey, blue, green } from '@material-ui/core/colors';
import './SortingContent.css';


export class SortingContent extends Component {

    render(props) {
        const lines = []
        this.props.items.forEach((item, idx) => {

            var backgroundColor = grey[500]
            var width = '4px'

            if (this.props.current === idx) {
                backgroundColor = green[500]
                width = '8px'
            } else if (this.props.highliths.includes(idx)) {
                backgroundColor = blue[500]
            }

            lines.push(<p
                key={item.key}
                style={
                    {
                        height: item.value + 'vh',
                        width: width,
                        backgroundColor: backgroundColor
                    }}
            ></p>)
        });

        return (
            <div className="SortingContent">
                {lines}
            </div>
        );
    }

}