import { Component } from "react";
import React from 'react';
import './SortingProcessTime.css';

export class SortingProcessTime extends Component {
    render(props) {
        if (this.props.time != null) {
            return (
                <div className="SortingProcessTime">
                    <p>Sorting time : <b>{this.props.time.toFixed(2)} milliseconds</b></p>
                </div>
            )
        } else {
            return (
                <div className="SortingProcessTime">
                    <p>Sorting time :</p>
                </div>
            )
        }
    }
}    
