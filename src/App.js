import React, { Component } from 'react';
import './App.css';
import { SortingContent } from './SortingContent/SortingContent';
import { SortingActions } from './SortingActions/SortingActions';
import { SortingProcessTime } from './SortingProcessTime/SortingProcessTime';
import { SortingAlgorithms } from './SortingAlgorithms/SortingAlgorithms';

export class App extends Component {

  // array of items used to calculate the processing time
  items = []

  // array of timeoutIds used to stop launched animations when needed 
  timeoutIds = []

  // array of animations to run
  animations = []

  state = {
    items: [],
    items_nb: 100,
    timeout: 3,
    running: false,
    stopped: false,
    highliths: [],
    current: null,
    time: null
  }

  constructor() {
    super()
    this.items = this.generateItems(this.state.items_nb)
  }

  componentDidMount() {
    this.setState({
      items: this.items
    })
  }

  /**
   * STOP button click
   */
  onStopClick() {
    this.stop()
  }

  /**
   * Generate new items click
   */
  onGenerateClick() {
    this.items = this.generateItems(this.state.items_nb)
    this.animations = []
    this.timeoutIds = []
    this.setState({
      items: this.items,
      current: [],
      highliths: [],
      stopped: false,
      time: null
    })
  }

  /**
   * items number slider change
   * @param value 
   */
  onItemsNbChange(value) {
    this.items = this.generateItems(value)
    this.setState({
      items_nb: value,
      items: this.items
    })
  }

  /**
   * animation timeout change
   * @param value 
   */
  onTimeoutChange(value) {
    this.setState({ timeout: value })
  }

  // set running state to true
  start() {
    this.setState({
      running: true
    })
  }

  // stopped state
  stop() {

    // clear running animations
    this.animations = []
    this.timeoutIds.forEach(timeoutId => {
      clearTimeout(timeoutId);
    })
    this.timeoutIds = []

    // update the state
    this.setState({
      running: false,
      stopped: true
    })
  }

  onBubbleSortClick() {
    SortingAlgorithms.BubbleSort(this)
    this.runAnimations()
  }

  onCocktailSortClick() {
    SortingAlgorithms.CocktailSort(this)
    this.runAnimations()
  }

  onQuickSortClick() {
    SortingAlgorithms.QuickSort(this)
    this.runAnimations()
  }

  runAnimations() {
    this.start()

    // iterate over each animation 
    this.animations.forEach((anim, idx) => {

      // timeout each anim
      var timeoutId = setTimeout(() => {

        var highliths = []

        if (anim.type === 'SWAP') {
          // swap items animation array
          SortingAlgorithms.swapElements(this.items, anim.data.first, anim.data.second)
        }

        else if (anim.type === 'QUICKSORT') {
          highliths = anim.data.partition
        }

        // update state and items
        this.setState({
          items: this.items,
          current: anim.data.current,
          highliths: highliths
        })

        // refresh UI with stop state 
        if (idx === this.animations.length - 1) {
          this.stop()
        }

      }, idx * this.state.timeout)

      // store each timeout to be able to stop the animation when needed.
      this.timeoutIds.push(timeoutId)

    })
  }

  /**
   * generate an array of items
   * @param {*} nbOfItems size
   */
  generateItems(nbOfItems) {
    var items = []
    for (var i = 0; i < nbOfItems; i++) {
      items.push({ key: i, value: this.randomSize() });
    }
    return items;
  }

  // generate a random number between 1 and 70
  randomSize() {
    const min = 1
    const max = 50
    return Math.floor(min + Math.random() * (max - min))
  }

  render(props) {
    return (

      <div className="App">

        <header className="App-header">
          <h1>Sorting algorithms visualizations</h1>
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        </header>

        <main className="App-content">
          <SortingActions
            running={this.state.running}
            stopped={this.state.stopped}
            onItemsNbChange={this.onItemsNbChange.bind(this)}
            onTimeoutChange={this.onTimeoutChange.bind(this)}
            onBubbleSortClick={this.onBubbleSortClick.bind(this)}
            onCocktailSortClick={this.onCocktailSortClick.bind(this)}
            onQuickSortClick={this.onQuickSortClick.bind(this)}
            onGenerateClick={this.onGenerateClick.bind(this)}
            onStopClick={this.onStopClick.bind(this)}>
          </SortingActions>

          <SortingContent
            items={this.state.items}
            highliths={this.state.highliths}
            current={this.state.current}>
          </SortingContent>

          <SortingProcessTime time={this.state.time}></SortingProcessTime>
        </main>

        <footer className="App-footer">
          <p>by <b>Etienne Pericat</b> -  2020 Coronavirus Quarantine coding</p>
        </footer>
      </div>
    )
  }

}

export default App;
