import { SortingAnimations } from "./SortingAnimations"

/**
 * Set of Sorting Algorithms methods.
 */
export class SortingAlgorithms {

    /**
     * Bubble sort entrypoint
     * 
     * @param {React.Component} app the parent component 
     */
    static BubbleSort(app) {

        const array = app.items.slice()
        var t0 = performance.now()

        for (let i = 0; i < array.length; i++) {
            for (let j = 0; j < array.length - 1; j++) {

                // swap itemps
                if (array[j + 1].value < array[j].value) {
                    this.swapElements(array, j, j + 1)
                    SortingAnimations.animateSwap(app, j, j + 1)
                }

                // animate each iteration
                SortingAnimations.animateBubbleSort(app, j)
            }
        }
        var t1 = performance.now()
        app.setState({ time: (t1 - t0) })

    }

    /**
     * Cocktail sort entrypoint
     * 
     * @param {React.Component} app the parent component 
     */
    static CocktailSort(app) {

        const array = app.items.slice()
        var t0 = performance.now()

        // cocktail sort
        var finish = false
        var start = 0
        var end = array.length - 1

        // animations variables
        while (!finish) {
            finish = true

            // ascending loop
            for (let i = start; i < end; i++) {

                // swap
                if (array[i].value > array[i + 1].value) {
                    finish = false
                    this.swapElements(array, i, i + 1)
                    SortingAnimations.animateSwap(app, i, i + 1)
                }

                SortingAnimations.animateCocktailSort(app, i)
            }

            // descending loop
            for (let j = end; j > start; j--) {

                if (array[j - 1].value > array[j].value) {
                    finish = false
                    this.swapElements(array, j, j - 1)
                    SortingAnimations.animateSwap(app, j, j - 1)
                }
                SortingAnimations.animateCocktailSort(app, j)

            }

            // at the end of each while loop -> biggest is sorted, lowest is sorted
            end--
            start++
        }

        var t1 = performance.now()
        app.setState({ time: (t1 - t0) })

    }

    /**
     * Quick Sort entrypoint. 
     * @param {React.Component} app the parent component
     */
    static QuickSort(app) {

        var t0 = performance.now()

        const array = app.items.slice()
        this.quickSort(array, 0, array.length - 1, app)

        var t1 = performance.now()
        app.setState({ time: (t1 - t0) })

    }

    /**
     * Recursive quick sort method.
     * 
     * @param {Array} array the array to sort
     * @param {Number} start the start of the partition to sort
     * @param {Number} end the end of the partition to sort
     * @param {React.Component} app the parent component 
     */
    static quickSort(array, start, end, app) {

        if (start < end) {
            var pi = this.quickSortPartition(array, start, end, app)
            this.quickSort(array, start, pi - 1, app)
            this.quickSort(array, pi + 1, end, app)
        }
    }

    /**
     * Partition of quick sort algorithm.
     * 
     * @param {Array} array the array
     * @param {Number} start the start index of the partition
     * @param {Number} end the end index of the partition
     * @param {React.Component} app the parent app component
     */
    static quickSortPartition(array, start, end, app) {

        // choose the pivot
        var pivot = array[end]
        var i = start

        // loop over the array
        for (var j = start; j <= end; j++) {

            // swap 
            if (array[j].value < pivot.value) {
                this.swapElements(array, j, i)
                SortingAnimations.animateSwap(app, j, i)
                i++
            }

            // add current animation
            SortingAnimations.animateQuickSort(app, j, array.slice(start, end).map((val, idx) => start + idx))
        }

        // swap last lowest with pivot
        this.swapElements(array, i, end)
        SortingAnimations.animateSwap(app, i, end)

        return i
    }

    /**
     * Swap 2 elements withtin the given array.
     * 
     * @param {Array} array 
     * @param {Number} first 
     * @param {Number} second 
     */
    static swapElements(array, first, second) {
        var temp = array[first]
        array[first] = array[second]
        array[second] = temp
        return array
    }

}