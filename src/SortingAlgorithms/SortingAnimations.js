/**
 * Set of methods used to animate 
 * multiple Sorting Algorithms {SortingAlgorithms class}
 */
export class SortingAnimations {

    /**
    * Add a swap animation in the queue
    * @param app the parent Component
    * @param first the first index to be swapped
    * @param second the second index to be swapped
    */
    static animateSwap(app, first, second) {
        app.animations.push({
            type: "SWAP",
            data: {
                first: first,
                second: second
            }
        })
    }

    /**
     * Add a quick sort animation to the queue
     * @param app 
     * @param index the current index
     * @param indexesArray the array of indexes that represents the partition
     */
    static animateQuickSort(app, index, indexesArray) {
        app.animations.push({
            type: "QUICKSORT",
            data: {
                current: index,
                partition: indexesArray
            }
        })
    }

    /**
     * Add a bubble sort animation to the queue
     * @param app 
     * @param index the current index
     */
    static animateBubbleSort(app, index) {
        app.animations.push({
            type: "BUBBLESORT",
            data: {
                current: index,
            }
        })
    }

    /**
     * Add a cocktail sort animation to the queue
     * @param app 
     * @param index the current index
     */
    static animateCocktailSort(app, index) {
        app.animations.push({
            type: "COCKTAILSORT",
            data: {
                current: index,
            }
        })
    }
}