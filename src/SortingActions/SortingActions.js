import { Component } from "react";
import React from 'react';
import { Button, IconButton } from '@material-ui/core';
import Slider from '@material-ui/core/Slider';
import Typography from '@material-ui/core/Typography';
import './SortingActions.css';
import { red, green } from '@material-ui/core/colors';
import { withStyles } from '@material-ui/core/styles';

import RefreshRoundedIcon from '@material-ui/icons/RefreshRounded';
import StopRoundedIcon from '@material-ui/icons/StopRounded';

// slider style
const CustomSlider = withStyles({
    root: {
        color: '#52af77',
        height: 8,
    },
    thumb: {
        height: 20,
        width: 20,
        backgroundColor: '#fff',
        border: '2px solid currentColor',
        marginTop: -8,
        marginLeft: -12,
        '&:focus, &:hover, &$active': {
            boxShadow: 'inherit',
        },
    },
    active: {},
    valueLabel: {
        left: 'calc(-50% + 4px)',
    },
    track: {
        height: 4,
        borderRadius: 4,
    },
    rail: {
        height: 4,
        borderRadius: 4,
    },
})(Slider);

export class SortingActions extends Component {

    timeoutChange(event, newValue) {
        this.props.onTimeoutChange(newValue)
    }

    itemsNbChange(event, newValue) {
        this.props.onItemsNbChange(newValue)
    }

    render(props) {
        return (
            <div className="actions">
                <IconButton
                    onClick={this.props.onGenerateClick}
                    className="sort"
                    variant="contained"
                    disabled={this.props.running}>
                    <RefreshRoundedIcon style={{ color: !this.props.running ? green[500] : green[50] }}></RefreshRoundedIcon>
                </IconButton>

                <Button
                    onClick={this.props.onBubbleSortClick}
                    className="sort"
                    variant="contained"
                    color="primary"
                    disabled={this.props.running || this.props.stopped}>
                    Bubble Sort
                </Button>

                <Button
                    onClick={this.props.onCocktailSortClick}
                    className="sort"
                    variant="contained"
                    color="primary"
                    disabled={this.props.running || this.props.stopped}>
                    Cocktail Sort
                </Button>

                <Button
                    onClick={this.props.onQuickSortClick}
                    className="sort"
                    variant="contained"
                    color="primary"
                    disabled={this.props.running || this.props.stopped}>
                    Quick Sort Lomuto
                </Button>

                <div className='space'></div>

                <div className='sliders'>
                    <div className='slider'>
                        <Typography>number of items</Typography>
                        <CustomSlider
                            max={200}
                            min={10}
                            onChange={this.itemsNbChange.bind(this)}
                            value={this.props.items_nb}
                            defaultValue={100}
                            step={10}
                            disabled={this.props.running}
                            marks={[{ value: 10, label: '10' }, { value: 100, label: '100' }, { value: 200, label: '200' }]}
                            valueLabelDisplay="auto"
                            aria-labelledby="discrete-slider"
                        />
                    </div>
                    <div className='slider'>
                        <Typography>slowness</Typography>
                        <CustomSlider
                            max={30}
                            min={3}
                            onChange={this.timeoutChange.bind(this)}
                            defaultValue={3}
                            step={1}
                            disabled={this.props.running}
                            marks={[{ value: 3, label: '3' }, { value: 30, label: '30' }]}
                            valueLabelDisplay="auto"
                            aria-labelledby="slider-timeout"
                        />
                    </div>
                </div>

                <IconButton
                    onClick={this.props.onStopClick}
                    className="sort"
                    variant="contained"
                    disabled={!this.props.running}>
                    <StopRoundedIcon style={{ color: this.props.running ? red[500] : red[100] }}></StopRoundedIcon>
                </IconButton>
            </div >
        )
    }
}